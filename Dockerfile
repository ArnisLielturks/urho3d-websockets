FROM ubuntu:18.04 as build
MAINTAINER Arnis Lielturks <arnis@example.com>

WORKDIR /var/source

# Install all the dependencies
RUN apt-get update \
    && apt-get install -y libgl1-mesa-dev git cmake g++ libx11-dev curl unzip golang zlib1g-dev\
    && apt-get purge --auto-remove -y && apt-get clean

COPY ./script Urho3D/script
COPY ./CMakeLists.txt Urho3D/CMakeLists.txt
COPY ./Docs Urho3D/Docs
COPY ./LICENSE Urho3D/LICENSE
COPY ./bin/Data Urho3D/bin/Data
COPY ./bin/CoreData Urho3D/bin/CoreData
COPY ./SourceOld Urho3D/Source
COPY ./CMake Urho3D/CMake

RUN cd Urho3D && \
    g++ --version && \
    bash script/cmake_generic.sh build -DURHO3D_SAMPLES=1 -DURHO3D_LUA=0 -DURHO3D_ANGELSCRIPT=0 -DURHO3D_TOOLS=0 -DURHO3D_HASH_DEBUG=0 -DURHO3D_PROFILING=0 -DURHO3D_64BIT=1 -DURHO3D_TESTING=0 -DURHO3D_DOCS=0 -DURHO3D_DEPLOYMENT_TARGET=generic && \
    cd build && make -j $(nproc) && \
    cd ..

COPY ./Source Urho3D/Source

RUN cd Urho3D && \
    g++ --version && \
    bash script/cmake_generic.sh build -DURHO3D_SAMPLES=1 -DURHO3D_LUA=0 -DURHO3D_ANGELSCRIPT=0 -DURHO3D_TOOLS=0 -DURHO3D_HASH_DEBUG=0 -DURHO3D_PROFILING=0 -DURHO3D_64BIT=1 -DURHO3D_TESTING=0 -DURHO3D_DOCS=0 -DURHO3D_DEPLOYMENT_TARGET=generic && \
    cd build && make -j $(nproc) && \
    cd ..

FROM hashicorp/consul-template:0.24.1-scratch as consul-template
FROM ubuntu:18.04
RUN apt-get update \
    && apt-get install -y libgl1-mesa-dev libx11-dev dumb-init \
    && apt-get purge --auto-remove -y && apt-get clean

RUN touch /SceneReplication.log
COPY ./config.hcl /config.hcl
COPY --from=consul-template /consul-template /usr/bin/consul-template

COPY --from=build /var/source/Urho3D/build/bin/17_SceneReplication /server
COPY --from=build /var/source/Urho3D/bin/Data /Data
COPY --from=build /var/source/Urho3D/bin/CoreData /CoreData

ENTRYPOINT ["/usr/bin/dumb-init", "--"]

CMD ["/bin/sh", "-c", "/usr/bin/consul-template -config=/config.hcl"]
